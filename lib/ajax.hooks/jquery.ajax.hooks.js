/*
  jquery.ajax.hooks - prefilter and postfilter ajax calls
  -------------------------------------------------------

    $.ajax.hooks.enable();
    $.ajax.hooks.disable();

    $.ajax.hooks({
      before: function(options) { ... },
      success: function(options) { ... }
    });

    $.ajax.hooks.before(function(options) {
        // @param options - $.ajax() single parameter
        // @return options - changed options to be processed by original $.ajax()
        //
    });

    $.ajax.hooks.success(function(data) {
        // @param data - ajax response data
        // @return object - result to be processed by original success
        //         undefined - original success should not be called
    });
*/

//
//
(function(root) {
  //
  var $ = root.jQuery;
  if(!$ || !$.ajax) {
    throw "You should include jquery.js before jquery.ajax.hooks.js";
  }

  //
  var _ = root._;
  if(!_) {
    throw "You should include underscore.js before jquery.ajax.hooks.js";
  }

  // Container for preserved functions
  //
  var saved = {
    ajax: undefined
  };

  // Arrays of before and success hooks
  //
  var hooks = {
    before: [],
    success: []
  };

  // Function to replace original $.ajax
  //
  var hookableAjax = function(opts) {
    var options = _.clone(opts);
    var overloadedSuccess;
    var savedSuccess;

    // Preprocess before
    //
    _.each(hooks.before, function(callback) {
      if(_.isFunction(callback)) {
        options = callback(options);
      }
    });

    // Overload success function
    //
    if(!_.isEmpty(hooks.success)) {
      //
      savedSuccess = options.success;

      overloadedSuccess = function(data, textStatus, jqXHR) {
        //
        var retData = _.clone(data);
        _.each(hooks.success, function(callback) {
          if(_.isFunction(callback)) {
            retData = callback(retData);
          }
        });

        if(retData && _.isFunction(savedSuccess)) {
          return savedSuccess(retData, textStatus, jqXHR);
        }
      };

      options.success = overloadedSuccess;
    }

    // Call original ajax function
    //
    saved.ajax(options);
  };

  //
  var enableAjaxHooks = function() {
    if(!saved.ajax) {
      saved.ajax = $.ajax;
      $.ajax = hookableAjax;
    }
  };

  //
  var disableAjaxHooks = function() {
    if(saved.ajax) {
      $.ajax = saved.ajax;
      saved.ajax = undefined;
    }
  }

  //
  var injectSuccessPrefilter = function(callback) {
    if(_.isFunction(callback)) {
      hooks.success.push(callback);
    }
  };

  //
  var injectBeforePrefilter = function(callback) {
    if(_.isFunction(callback)) {
      hooks.before.push(callback);
    }
  };

  //
  var noop = function() {};

  //
  var injectCSRFHandler = function() {
    // Insert authenticity_token into ajax call
    injectBeforePrefilter(function(options) {
      // TODO: check for ajax http method, use for post only

      // Initialize, if empty
      options.data || (options.data = {});

      //
      options.data.authenticity_token = $("meta[name=csrf-token]").attr("content");
      options.data.utf8 = "\u2713"; // check symbol

      return options;
    });

    // Replace page authenticity_token, if specified in result data
    injectSuccessPrefilter(function(data) {
      var token = (typeof(data.status) == "object") ? data.status.authenticity_token : undefined;
      if(token) {
        delete data.authenticity_token;
        $("meta[name=csrf-token]").attr("content", token);
      }
      return data;
    });
  };

  // After first call won't be available
  //
  $.ajax.hooks = function(options) {
    injectBeforePrefilter(options.before);
    injectSuccessPrefilter(options.success);
    enableAjaxHooks();

    return hookableAjax.hooks;
  };

  $.ajax.hooks.enable  = enableAjaxHooks;
  $.ajax.hooks.success = injectSuccessPrefilter;
  $.ajax.hooks.before  = injectBeforePrefilter;
  $.ajax.hooks.csrf    = injectCSRFHandler;

  //
  hookableAjax.hooks = {
    disable: disableAjaxHooks,
    success: injectSuccessPrefilter,
    before:  injectBeforePrefilter,
    csrf:    injectCSRFHandler
  };

})(this);
