describe("Form.Hash", function() {
  var node;
  var formHash;

  beforeEach(function() {
    node = {attr: function() {}};
  })

  describe("creation", function() {
    var value;

    beforeEach(function() {
      value = "green";
    });

    describe("from node with \\w+ name", function() {
      beforeEach(function() {
        spyOn(node, "attr").andReturn("foobar");
      });

      it("should create hash with `foobar' key and `green' value", function() {
        formHash = new Form.Hash(node, value);
        expect(formHash.hash).toEqual({foobar: "green"});
      });
    });

    describe("from node with \\w+[] name", function() {
      beforeEach(function() {
        spyOn(node, "attr").andReturn("foobar[]");
      });

      it("should create hash with `foobar' key and value as array == ['green']", function() {
        formHash = new Form.Hash(node, value);
        expect(formHash.hash.foobar.array).toEqual(["green"]);
      });
    });
  });

  describe("#merge", function() {
    var otherHash;
    var hashValue = {a: 3};
    var returnValue;

    beforeEach(function() {
      otherHash = {hash: {a: 1, b: 2}};
      returnValue = _.clone(hashValue);
      spyOn(node, "attr").andReturn("a");
      formHash = new Form.Hash(node, 3);
      spyOn(formHash, "mergeElement").andCallFake(function() {
        returnValue.a += 1;
        return returnValue;
      });
    });

    it("should call mergeElement with all otherHash pairs", function() {
      formHash.merge(otherHash);

      expect(formHash.mergeElement).toHaveBeenCalledWith(hashValue, "a", 1);
      expect(formHash.mergeElement).toHaveBeenCalledWith(returnValue, "b", 2);
    });

    it("should assign this.hash with return value from last #mergeElement call", function() {
      expect(formHash.merge(otherHash).hash).toEqual({a: 5});
    });
  });

  describe("#mergeElement", function() {
    var hash;
    var key;

    beforeEach(function() {
      spyOn(node, "attr").andReturn("foobar");
    });

    describe("existing key", function() {
      var mock;

      beforeEach(function() {
        key = "foo";
        mock = {merge: function() {}};
        hash = {foo: mock};
        spyOn(mock, "merge").andReturn({foo: "value"});

        formHash = new Form.Hash(node, {foo: "zzz"});
      });

      it("should merge with same key", function() {
        formHash.mergeElement(hash, key, "value");
        expect(mock.merge).toHaveBeenCalledWith("value");
      });
    });

    describe("new key", function() {
      beforeEach(function() {
        hash = {foo: "bar"};
        key  = "baz";
      });

      it("should be assigned with new key and value", function() {
        formHash.mergeElement(hash, key, "value");
        expect(hash[key]).toEqual("value");
      });
    });
  });

  describe("#toJSON", function() {
    beforeEach(function() {
      var n1 = {toJSON: function() {return "n1";}};
      var n2 = {toJSON: function() {return "n2";}};

      spyOn(node, "attr").andReturn("foobar");

      formHash = new Form.Hash(node, "foo");
      formHash.hash = {a: n1, b: n2, c: 3, d: "value"};
    });

    it("should collect #toJSON from values", function() {
      expect(formHash.toJSON()).toEqual({a: "n1", b:"n2", c: 3, d: "value"});
    });
  });
});
