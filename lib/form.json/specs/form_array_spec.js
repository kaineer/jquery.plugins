describe("Form.Array", function() {
  var formArray;
  var stubNode;

  beforeEach(function() {
    stubNode = [12345];
  });

  describe("creation", function() {
    describe("with scalar value", function() {
      beforeEach(function() {
        formArray = new Form.Array(stubNode, "abcdef");
      });

      it("should contain single element", function() {
        expect(formArray.array).toEqual(["abcdef"]);
      });

      it("should keep node", function() {
        expect(formArray.node).toEqual(stubNode);
      });
    });

    describe("with object as value", function() {
      var value;

      beforeEach(function() {
        value = {setParentNode: function() {}};
        spyOn(value, "setParentNode").andCallFake(function() {});
        formArray = new Form.Array(stubNode, value);
      });

      it("should call value.setParentNode()", function() {
        expect(value.setParentNode).toHaveBeenCalledWith(stubNode);
      });

      it("should have that object in an array", function() {
        expect(formArray.array).toEqual([value]);
      });
    });
  });

  describe("#merge", function() {
    var other = {array: [1, 2, 3]};
    var fb;
    var noop;

    describe("enumeration", function() {
      beforeEach(function() {
        other = {array: [1, 2, 3]};
        fb = ["foobar"];
        noop = function() {return fb;};

        formArray = new Form.Array(stubNode, "foobar");
        spyOn(formArray, "mergeElement").andCallFake(noop);
      });

      it("should call mergeElement with each element from incoming array", function() {
        formArray.merge(other);

        expect(formArray.mergeElement).toHaveBeenCalledWith(fb, 1);
        expect(formArray.mergeElement).toHaveBeenCalledWith(fb, 2);
        expect(formArray.mergeElement).toHaveBeenCalledWith(fb, 3);
      });
    });

    describe("result", function() {
      beforeEach(function() {
        other = {array: [1]};
        fb = [1, 2, 3, 4, 5];
        noop = function() {return fb;}

        formArray = new Form.Array(stubNode, "foobar");
        spyOn(formArray, "mergeElement").andReturn(fb);
      });

      it("should assign last #mergeElement call result to this.array", function() {
        formArray.merge(other);
        expect(formArray.array).toEqual(fb);
      });
    });
  });

  describe("#mergeElement", function() {
    describe("merging objects", function() {
      var createNode = function(parent) {
        return {
          parentNode: parent,
          setParentNode: function() {},
          merge: function() {}
        };
      };

      var n1 = createNode(1);
      var n2 = createNode(2);
      var n3 = createNode(3);
      var n11 = createNode(1);
      var n41 = createNode(4);
      var nodesArray;
      var resultArray;

      beforeEach(function() {
        formArray = new Form.Array(stubNode, n1);
        nodesArray = [n1, n2, n3];
        spyOn(n1, "merge").andCallFake(function() {});
        spyOn(nodesArray, "push").andCallFake(function() {return nodesArray;});
      });

      it("should merge with same parent node", function() {
        formArray.mergeElement(nodesArray, n11);
        expect(n1.merge).toHaveBeenCalledWith(n11);
      });

      it("should be pushed into result array, if no same parent node found", function() {
        formArray.mergeElement(nodesArray, n41);
        expect(nodesArray.push).toHaveBeenCalledWith(n41);
      });

      it("should return result array", function() {
        expect(formArray.mergeElement(nodesArray, n41)).toEqual(nodesArray);
      });
    });

    describe("merging strings", function() {
      beforeEach(function() {
        formArray = new Form.Array(stubNode, "yellow");
      });

      it("should push new string in", function() {
        expect(formArray.mergeElement(["yellow"], "green")).toEqual(["yellow", "green"]);
      });
    });
  });

  describe("#toJSON", function() {
    beforeEach(function() {
      var n1 = {toJSON: function() {return "n1";}};
      var n2 = {toJSON: function() {return "n2";}};

      formArray = new Form.Array(stubNode, "foo");
      formArray.array = [n1, n2, 3, "bar"];
    });

    it("should collect #toJSON from objects and values from scalars", function() {
      expect(formArray.toJSON()).toEqual(["n1", "n2", 3, "bar"]);
    });
  });
});
