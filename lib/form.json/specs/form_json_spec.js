describe("Form.JSON", function() {
  var formHTML;
  var formJSON;
  var node;

  describe("test environment", function() {
    beforeEach(function() {
      formHTML = "<form><input /></form>";
      node = $(formHTML).find("input");
    });

    it("should have jQuery defined", function() {
      // if this fails, you should reinclude jQuery somewhere
      expect($).not.toBeUndefined();
    });

    it("should not be undefined", function() {
      expect(typeof(node)).not.toBeUndefined();
    });
  }); // test environment

  describe("#inputHash", function() {
    // Recreate context for #inputHash
    var fetchInputHash = function(formHtml, inputSelector) {
      node = $(formHtml).find(inputSelector);
      formJSON = (new Form.JSON({})).inputHash(node, {});
      return formJSON.toJSON();
    };

    describe("form with input", function() {
      beforeEach(function() {
        formJSON = fetchInputHash(
          "<form name='user'><input name='email' value='q@a.s' type='text'/></form>",
          "input");
      });

      // debug
      it("should not be undefined", function() {
        expect(formJSON).not.toBeUndefined();
      });

      it("should contain user's email", function() {
        expect(formJSON.user.email).toEqual("q@a.s");
      });
    });

    describe("Form with checkbox", function() {
      beforeEach(function() {
        formJSON = fetchInputHash(
          "<form name='user'><input type='checkbox' value='1' name='number[]' /></form>",
          "input"
        );
      });

      it("should contain array", function() {
        expect(formJSON.user.number).toEqual(["1"]);
      });
    });

    describe("form, fieldset, input", function() {
      beforeEach(function() {
        formJSON = fetchInputHash(
          "<form name='user'><fieldset name='address'><input name='street' value='Greenest' /></form>",
          "input"
        );
      });

      it("should contain address's street name", function() {
        expect(formJSON.user.address.street).toEqual("Greenest");
      });
    });

    describe("fieldset, form, input", function() {
      beforeEach(function() {
        formJSON = fetchInputHash(
          "<fieldset name='account'><form name='user'><input name='name' value='John' /></form></fieldset>",
          "input"
        );
      });

      it("should skip outer fieldset", function() {
        expect(formJSON.user.name).toEqual("John");
        expect(formJSON.account).toBeUndefined();
      });
    });
  }); // #inputHash

  describe("#calculateJSON", function() {
    var fetchCalculatedJSON = function(html, selector, options) {
      var html = arguments[0];
      var selector = arguments[1];
      var options;

      if(arguments.length > 2) {
        options = arguments[arguments.length - 1];
      } else {
        options = selector;
        selector = null;
      }

      options || (options = {});
      nodeset = $(html);
      if(selector) {
        nodeset = nodeset.find(selector);
      }

      return (new Form.JSON(nodeset, options)).calculateJSON().toJSON();
    };

    describe("Simple form", function() {
      beforeEach(function() {
        formJSON = fetchCalculatedJSON(
          "<div><form id='simple' name='user'><input name='first_name' value='John'/><input name='last_name' value='Gladstone'/></form></div>",
          "#simple",
          {inputsSelector: "input"}
        );
      });

      it("should contain first/last name of user", function() {
        expect(formJSON).toEqual({user: {first_name: "John", last_name: "Gladstone"}});
      });
    });

    describe("Form with decoration fieldset", function() {
      beforeEach(function() {
        formJSON = fetchCalculatedJSON(
          "<div><form id='decorated' name='user'><fieldset><input name='name' value='John' /></fieldset></div>",
          "#decorated",
          {inputsSelector: "input"}
        );
      });

      it("should contain user name", function() {
        expect(formJSON).toEqual({user: {name: "John"}});
      });
    });

    describe("Form with array fieldset", function() {
      beforeEach(function() {
        formJSON = fetchCalculatedJSON(
          "<div><form id='repetitions' name='user'><fieldset name='pocket[]'><input name='size' value='10in' />" +
          "<input name='content' value='Magic beans' /></fieldset></form></div>",
          "#repetitions",
          {inputsSelector: "input"}
        );
      });

      it("should have array with single pocket inside", function() {
        expect(formJSON).toEqual({user: {pocket: [ {size: '10in', content: 'Magic beans'} ] } });
      });
    });

    describe("Form with array with two elements", function() {
      beforeEach(function() {
        formJSON = fetchCalculatedJSON(
          "<div><form id='repetitions' name='user'><fieldset name='pocket[]'><input name='size' value='10in' />" +
          "<input name='content' value='Magic beans' /></fieldset><fieldset name='pocket[]'>" +
          "<input name='size' value='12in'/><input name='content' value='Green cheeze'/>" +
          "</fieldset></form></div>",
          "#repetitions",
          {inputsSelector: "input"}
        );
      });

      it("should have array with two hash elements", function() {
        expect(formJSON).toEqual({user: {pocket: [ {size: '10in', content: 'Magic beans'}, {size: '12in', content: 'Green cheeze'} ] } });
      });
    });

    describe("Form with checkboxes array", function() {
      beforeEach(function() {
        formJSON = fetchCalculatedJSON(
          "<div><form id='checkboxes' name='user'><input name='number[]' value='1'/><input name='number[]' value='2'/></form></div>",
          "#checkboxes",
          {inputsSelector: "input"}
        );
      });

      it("should have array with two string elements", function() {
        expect(formJSON).toEqual({user: {number: ["1", "2"]}});
      });
    });
  }); // #calculateJSON

  describe("$.fn.formJSON()", function() {
    beforeEach(function() {
      formHTML = "<form name='user'><input type='text' name='name' value='John'></form>";
    });

    it("should return correct json", function() {
      expect($(formHTML).formJSON()).toEqual({user: {name: "John"}});
    });
  }); // $.fn.formJSON()
});
