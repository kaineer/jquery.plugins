//
//
//
//
//
//

(function(root) {
  // Regexp to detect arrays names
  //
  var FormArrayNameRE = /^(\w+)\[\]$/;

  // Is element an object
  //
  var isElement = function(element) {
    return element && typeof(element) == "object";
  };

  // Classes' namespace
  //
  var Form = {
    // Array with reference to node, where it was created
    //
    Array: function(node, value) {
      this.node = node.get ? node.get(0) : node;

      if(isElement(value)) {
        value.setParentNode(this.node);
      }

      this.array = [value];

      return this;
    },

    // Hash with optional parent node reference
    //
    Hash: function(node, value) {
      var name = node.attr("name");
      var matchData;

      this.hash = {};

      if(value) {
        if(matchData = name.match(FormArrayNameRE)) {
          name = matchData[1];
          value = new Form.Array(node, value);
        }

        this.hash[name] = value;
      }

      return this;
    },

    JSON: function(nodeset, options) {
      this.options = options ? _.clone(options) : {};
      this.nodeset = nodeset;
      return this;
    }
  }; // Form classes

  Form.Array.prototype = {
    setParentNode: function(node) {
      this.node = node;
    },

    merge: function(other) {
      var that = this;

      this.array = _.reduce(other.array, function(array, element) {
        return that.mergeElement(array, element);
      }, _.clone(this.array));

      return this;
    },

    mergeElement: function(array, newElement) {
      if(typeof(newElement) == "object") {
        var destination = _.find(array, function(elt) {
          return elt.parentNode == newElement.parentNode;
        });

        if(destination) {
          destination.merge(newElement);
        } else {
          array.push(newElement);
        }
      } else {
        array.push(newElement);
      }

      return array;
    },

    toJSON: function() {
      return _.map(this.array, function(element) {
        return isElement(element) ? element.toJSON() : element;
      });
    }

  }; // Form.Array.prototype

  Form.Hash.prototype = {
    setParentNode: function(node) {
      this.parentNode = (node.get ? node.get(0) : node);
    },

    merge: function(other) {
      var that = this;

      this.hash = _.reduce(other.hash, function(hash, element, key) {
        return that.mergeElement(hash, key, element);
      }, _.clone(this.hash));

      return this;
    },

    mergeElement: function(hash, key, value) {
      var element = hash[key];

      if(isElement(element)) {
        element.merge(value);
      } else {
        hash[key] = value;
      }

      return hash;
    },

    toJSON: function() {
      return _.reduce(this.hash, function(hash, element, key) {
        hash[key] = isElement(element) ? element.toJSON() : element;
        return hash;
      }, {});
    }
  }; // Form.Hash.prototype

  //
  // Form.JSON
  //
  var keyValue = function(node, prevValue) {
    if(node.attr("name")) {
      return new Form.Hash(node, prevValue || node.val());
    } else {
      return prevValue;
    }
  };

  //

  var extractAutenticityToken = function(json) {
    if(json.utf8 && json.authenticity_token) {
      return json;
    } else {
      var node;
      for(var key in json) {
        node = json[key];
        if(node.utf8 && node.authenticity_token) {
          json.utf8 = node.utf8;
          delete node.utf8;
          json.authenticity_token = node.authenticity_token;
          delete node.authenticity_token;
          break;
        }
      }
      return json;
    }
  };

  Form.JSON.defaults = {
    inputsSelector: "input:text, input:checked, textarea, select, input:hidden",
    parentsSelector: "fieldset, form"
  };

  Form.JSON.prototype = {
    // TODO: make a spec
    calculateJSON: function() {
      var inputsSelector = this.options.inputsSelector || Form.JSON.defaults.inputsSelector;
      var result = new Form.Hash(this.nodeset); // NOTE: what's that?
      var that = this;

      this.nodeset.find(inputsSelector).each(function() {
        console.log("node");
        result = result.merge(that.inputHash($(this)));
      });

      return result;
    },

    // TODO: make a spec, start here
    inputHash: function(inputNode) {
      var parentsSelector = this.options.parents || Form.JSON.defaults.parentsSelector;
      var parentNodes = inputNode.parents(parentsSelector);
      var result = keyValue(inputNode);

      for(var i in parentNodes) {
        var parent = parentNodes[i];
        result = keyValue($(parent), result);

        if(parent.tagName.toLowerCase() == "form") {
          // TODO: break?
          return result;
        }
      }

      return result;
    },

    toJSON: function() {
      this.json || (this.json = this.calculateJSON());
      this.json = extractAutenticityToken(this.json);
      return this.json.toJSON();
    }
  }; // Form.JSON.prototype

  //
  // Entry point
  //
  $.fn.formJSON = function() {
    var data = new Form.JSON(this);
    return data.toJSON();
  };

  // export for testing purposes
  if(typeof(root.jasmine) == "object") {
    root.Form = Form;
  }
})(this);
